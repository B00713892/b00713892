print("Programme to work out division of two array numbers")
print("********************************************")
print("Answer")
top_number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
              29, 30]
bottom_number = [30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4,
                 3, 2, 1]
calculation_answer = []

for i in range(30):
    top = top_number[i]
    bottom = bottom_number[i]
    answer = bottom / top
    calculation_answer.append(answer)
    print(str(calculation_answer[i]))



