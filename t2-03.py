print("Programme to print pattern utilizing user input")

rows = int(input("Please enter the number of rows"))

for i in range(0, rows):
    for j in range(0, i + 1):
        print("*", end=' ')

    print("\r")
