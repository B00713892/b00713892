import random

roll_amount = 500
snake_eyes_amount = 0
for x in range(roll_amount):
    dice_1 = random.randint(1, 6)
    dice_2 = random.randint(1, 6)
    if (dice_1 == 1) & (dice_2 == 1):
        snake_eyes_amount += 1
        print("LOOP " + str(x) + ": " + str(dice_1) + " & " + str(dice_2) + " SNAKE EYES!")

    else:
        print("LOOP " + str(x) + ": " + str(dice_1) + " & " + str(dice_2))

print("Number of times snake eyes is rolled is " + str(snake_eyes_amount))
