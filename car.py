print("Practical lab 4.2")

class Car:

    def __init__(self, make, year):
        self.__year__ = year
        self.__make__ = make
        self.__speed__ = 0

    def accelerate(self, speed_step):
        self.__speed__ += speed_step
        print("Speed increased")

    def brake(self, reduce_speed_step):
        self.__speed__ -= reduce_speed_step
        print("Speed Decreased")

    def get_speed(self):
        print("The current speed is" + str(self.__speed__))
