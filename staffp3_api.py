from flask import (
    Flask,
    jsonify,
    request, abort
)

import mysql.connector

VERSION = 'v1'
BASE_URL = '/api/' + VERSION
STAFFS = '/staffs'
STAFF = '/staff'
url_all_staffs = BASE_URL + STAFFS

app = Flask(__name__, )


@app.route(url_all_staffs, methods=['POST'])
def add_staffs():
    if not request.json:
        abort(400)
    cnx = mysql.connector.connect(user='B00713892',
                                  password='zyBVM1kr',
                                  host='scm.ulster.ac.uk/mysql',
                                  port='3307',
                                  database='B00713892')
    cursor = cnx.cursor()
    sql = "INSERT INTO staff (FirstName, LastName, Campus) ", "values (%s,%s, %s)"

    cursor.execute(sql, (request.json['FirstName'],
                         request.json['LastName'],
                         request.json['Campus']))
    id = cursor.lastrowid
    cnx.commit()

    sql= "SELECT * from staff where staff_id=" + str(id)
    cursor.execute(sql)
    row = cursor.fetchone()
    staff = {}
    for (key, value) in zip(cursor.description, row):
        staff[key[0]] = value
    cnx.close()

    return jsonify(staff), 201


if __name__ == '__main__':
    app.run(debug=True)
