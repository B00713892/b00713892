from car import Car

my_car = Car("Fiat", 2001)
my_car_1 = Car("Fiat", 2002)
my_car_2 = Car("Fiat", 2003)
my_car_3 = Car("Fiat", 2004)
my_car_4 = Car("Fiat", 2005)

my_car.accelerate(10)
my_car.brake(5)
my_car.get_speed()
