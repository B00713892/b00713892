from flask import (
    Flask,
    jsonify,
    request, abort
)

import mysql.connector

VERSION = 'v1'
BASE_URL = '/api/' + VERSION
STAFFS = '/staffs'
STAFF = '/staff'
url_all_staffs = BASE_URL + STAFFS

app = Flask(__name__,)


@app.route(url_all_staffs, methods=['GET'])
def get_staffs():
    cnx = mysql.connector.connect(user='B00713892',
                                  password='zyBVM1kr',
                                  host='scm.ulster.ac.uk/mysql',
                                  database='B00713892',
                                  port='3306')
    cursor = cnx.cursor()
    query = ("SELECT * FROM staff")
    cursor.execute(query)
    rows = cursor.fetchall()
    items = []
    for row in rows:
        dict = {}
        for (key, value) in zip(cursor.description, row):
            dict[key[0]] = value
        items.append(dict)
    cnx.close()

    return jsonify({'staffs': items}), 200


if __name__ == '__main__':
    app.run(debug=True)
