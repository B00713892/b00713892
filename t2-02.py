amount = int(input("How many numbers do you wish to enter?"))

numbers = []
answer = 1
for i in range(amount):
    if answer == 1:
        number = int(input("Please enter a number"))
        if (number % 2) == 0:
            numbers.append(number)
        else:
            answer = 0
            print("You can only enter even numbers")

Total = sum(numbers)
print("The Sum of all entered numbers is" + str(Total))
